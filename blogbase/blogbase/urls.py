"""blogbase URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.auth import views
from filebrowser.sites import site
from django.contrib import admin

#name space defines the name used in your links when calling
#and looping through posts and categories, as dsblog, if you change this
#you also need to change the links where it calls a hef ref... 'dsblog:post_list

#also need to ensure that the include urls matches the app urls location
#and again ensure that after r'', there is nothing so not to keep looking
#and accepting any url path that is entered

urlpatterns = [
    url(r'^where_the_magic_happens/', include(admin.site.urls)),
    url(r'^admin/', include('admin_honeypot.urls', namespace='admin_honeypot')),
    url(r'', include('dsblog.urls', app_name='dsblog', namespace='dsblog')),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^where_the_magic_happens/filebrowser/', include(site.urls)),
]

if settings.DEBUG is True:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
