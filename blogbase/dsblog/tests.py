from django.test import TestCase
from django.db import models
from django import forms
from django.utils import timezone
from tinymce import HTMLField
from django.template.defaultfilters import slugify

from .models import Category

class CategoryTest(TestCase):

    def test_title_string(self):
        entry = Category(title="test")
        self.assertEqual(str(entry), entry.title)
