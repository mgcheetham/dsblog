from .views import *
from django.conf.urls import *
from django.conf import settings
from django.views.generic.base import TemplateView
from .views import sendmail

#shows the feedback form page in conjunction with the url and view.
#works with the url in feedback form
app_name='form'

urlpatterns = [
    url(r'^$', post_list, name="post_list"),
    url(r'^post/(?P<slug>[-\w]+)/$', post_detail, name="post_detail"),
    url(r'^category/(?P<slug>[-\w]+)/$', category_detail, name="category_detail"),
    url(r'^email/send/$', sendmail),
    url(r'^email/thankyou/$', TemplateView.as_view(template_name="thankyou.html"), name="thankyou"),
    url(r'^email/$', TemplateView.as_view(template_name="email.html"), name="email"),
    url(r'^images/$', images, name="images"),
    url(r'^astro/$', astro, name="astro"),
    url(r'^crypto/$', crypto, name="crypto"),
    url(r'^about/$', about, name="about"),
    url(r'^privacypolicy/$', privacy_policy, name="privacy_policy"),
    url(r'^commentpolicy/$', comment_policy, name="comment_policy"),
    url(r'^results/$', search, name="search"),
    url(r'^matt-cheetham-cv-2018/$', cv, name="cv"),
]

#the name for post detail and category detail matches the a href links
#in the templates. Also the slug is passed as an argument into the Links

#also need to make sure the post list just has ^$ otherwise you can keep
#typing any urlpath into the browser bar and it'll match it
#it says to django to stop looking at the raw string
