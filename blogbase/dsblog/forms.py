from django import forms
from .models import *
from django.db import models

#works with the email model to setup email forms, and use the variables
class ContactForm(forms.Form):
    contact_name = forms.CharField(required=True)
    contact_email = forms.EmailField(required=True)
    content = forms.CharField(required=True, widget=forms.Textarea)

#meta takes the feedback model so there's no need to duplicate variables
#model assigns to which model and fields defines which fields will be shown in the templates
#exclude does the opposite
class FeedbackForm(forms.ModelForm):

    class Meta:
        model = Feedback
        fields = ['company', 'position_feedback', 'cv_feedback']
        exclude = ['date']
