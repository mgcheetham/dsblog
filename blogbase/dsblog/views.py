from django.shortcuts import render, get_object_or_404
from .models import *
from django.utils import timezone
from django.shortcuts import redirect
from .forms import *
from django.core.mail import EmailMessage
from django.template.loader import get_template
from django.template import Context
from django.views.generic.base import TemplateView
from django.http import HttpResponseRedirect
from django.core.mail import send_mail, BadHeaderError
from django.db.models import Q
from blogbase.config import *

#defines the post list view that returns the post_list.html as filtered by published
#the objects_list variable is what gets called in the post detail and category a href links in the templates
#if the variable changes please update links

#pagination also is now seperated into it's own file within blogbase.config
#num=5 shows 5 posts per page, pages under context is set at 1 amending this creates an error
#also need to have the more posts than the num=5 minimum otherwise it won't display the links
def post_list(request):
    template = 'blog/post_list.html'
    objects_list = Post.objects.filter(status='Published',
        published_date__lte=timezone.now()).order_by('-published_date')

    pages = pagination(request, objects_list, num=5)

    context = {
        'items': pages[0],
        'page_range': pages[1],
    }
    return render(request, template, context)


#defines the post detail view
def post_detail(request, slug):
    template = 'blog/post_detail.html'

    post = get_object_or_404(Post, slug=slug)
    context = {
        'post': post,
    }
    return render(request, template, context)


#defines the category detail view, now passes in 2 parameters incl slug
#which need to be configured under views and urls
def category_detail(request, slug):
    template = 'blog/category_detail.html'
    #category argument only shows category
    category = get_object_or_404(Category, slug=slug)
    post = Post.objects.filter(category=category)

    context = {
    'category': category,
    'post': post,
    }
    return render(request, template, context)


#defines search function with the q method icontains creates the search
def search(request):
    template = 'blog/post_list.html'

    query = request.GET.get('q')

    if query:
        results = Post.objects.filter(Q(title__icontains=query) | Q(text__icontains=query))
    else:
        results = Post.objects.filter(status="Published")

    pages = pagination(request, results, num=5)

    context = {
        'items': pages[0],
        'page_range': pages[1],
        'query': query,
    }
    return render(request, template, context)


#defines the send email logic and works with the .forms 
def sendmail(request):
      if request.method == 'POST':
        form = EmailForm(request.POST)
        if form.is_valid():
          firstname = form.cleaned_data['firstname']
          lastname = form.cleaned_data['lastname']
          email = form.cleaned_data['email']
          subject = form.cleaned_data['subject']
          botcheck = form.cleaned_data['botcheck'].lower()
          message = form.cleaned_data['message']
          if botcheck == 'yes':
            try:
              fullemail = firstname + " " + lastname + " " + "<" + email + ">"
              send_mail(subject, message, fullemail, ['matt.cheetham@darkskiesblog.co.uk'])
              return HttpResponseRedirect('/email/thankyou/')
            except:
              return HttpResponseRedirect('/email/')
        else:
          return HttpResponseRedirect('/email/')
      else:
        return HttpResponseRedirect('/email/')


#defines links to pages, returns and renders pages
def images(request):
    return render(request, 'blog/images.html', {})

def astro(request):
    return render(request, 'blog/astro.html', {})

def crypto(request):
    return render(request, 'blog/crypto.html', {})

def about(request):
    return render(request, 'blog/about.html', {})

def privacy_policy(request):
    return render(request, 'blog/privacy_policy.html', {})

def comment_policy(request):
    return render(request, 'blog/comment_policy.html', {})

#defines the cv view including the feedback form
def cv(request):

    if request.method == "POST":
        form = FeedbackForm(request.POST)

        if form.is_valid():
            company = form.cleaned_data['company']
            position_feedback = form.cleaned_data['position_feedback']
            cv_feedback = form.cleaned_data['cv_feedback']
            form.save()
            return render(request, 'blog/cv.html')

    else:
        form = FeedbackForm()

    return render(request, 'blog/cv.html', {'form': form})
