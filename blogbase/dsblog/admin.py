from django.contrib import admin
from .models import *

#modifies the admin page to display more information on categories
class CategoryAdmin(admin.ModelAdmin):
    exclude = ('slug',)
    list_display = ('title', 'created', 'updated',)
admin.site.register(Category, CategoryAdmin)

#modifies the admin page to display more information on posts
class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'tagline', 'status', 'created_date', 'published_date',)

admin.site.register(Post, PostAdmin)

#modifies the admin page to display form feedback
class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('company', 'position_feedback', 'cv_feedback', 'date')

    class Meta:
        model = Feedback

admin.site.register(Feedback, FeedbackAdmin)
