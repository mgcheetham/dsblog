from django.db import models
from django import forms
from django.utils import timezone
from tinymce import HTMLField
from django.template.defaultfilters import slugify

#model for category creation in the database
class Category(models.Model):
    title = models.CharField(max_length=250)
    description = models.CharField(max_length=250)
    slug = models.SlugField(max_length=200, unique=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    # save function returns slug with title
    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Category, self).save(*args, **kwargs)
    #returns the title as a string value
    def __str__(self):
        return self.title
    #non-field type - sets plural name for category
    class Meta:
        verbose_name_plural = "Categories"

#model for post creation in the database
class Post(models.Model):
    #choices in tuple form iteratable and shows select box instead
    STATUS_CHOICES = (
        ('Published', 'Published'),
        ('Draft', 'Draft'),
    )
    author = models.ForeignKey('auth.User')
    title = models.CharField(max_length=200)
    text = HTMLField('Content')
    tagline = models.TextField()
    image = models.ImageField()
    category = models.ForeignKey(Category)
    slug = models.SlugField(max_length=200, unique=True)
    status = models.CharField(max_length=10, default='Draft', choices=STATUS_CHOICES)
    created_date = models.DateTimeField(auto_now_add=True)
    published_date = models.DateTimeField(default=timezone.now)
    #args/kwargs unpacks all potential arguments
    #ensure super class touches the database and saves the object
    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Post, self).save(*args, **kwargs)
    #displays title as string
    def __str__(self):
        return self.title

#model for email form creation in the database
class EmailForm(forms.Form):
      firstname = forms.CharField(max_length=255)
      lastname = forms.CharField(max_length=255)
      email = forms.EmailField()
      subject = forms.CharField(max_length=255)
      botcheck = forms.CharField(max_length=5)
      message = forms.CharField()

#model for form feedback creation in the database
class Feedback(models.Model):

    company = models.CharField(max_length=50)
    position_feedback = models.TextField(max_length=255)
    cv_feedback = models.TextField(max_length=255)
    date = models.DateTimeField(auto_now_add=True)
    #displays company as string
    def __str__(self):
        return self.company
    #sets admin page plural name
    class Meta:
        verbose_name_plural = "Feedback"
